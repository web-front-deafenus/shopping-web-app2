import {Component, OnInit} from '@angular/core';
// import * as firebase from 'firebase'
import firebase from "firebase/app"

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'shopping-web-app2';

  ngOnInit() {
  }
}

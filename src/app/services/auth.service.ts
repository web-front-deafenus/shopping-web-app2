import {Injectable} from '@angular/core';
import * as firebase from 'firebase'

// import auth from 'firebase/app'
import {AngularFirestore} from "@angular/fire/firestore";
import {AngularFireAuth} from "@angular/fire/auth";
import {Router} from "@angular/router";
import {NgZone} from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  userData: any;

  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,
    public ngZone: NgZone // NgZone service to remove outside scope warning
  ) {
    /* Saving user data in localstorage when
    logged in and setting up null when logged out */
    /*this.afAuth.authState.subscribe(user => {
      if (user) {
        this.userData = user;
        localStorage.setItem('user', JSON.stringify(this.userData));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    })*/
  }

  // Sign in with email/password
  singinUser(email: string, password: string) {
    return this.afAuth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        console.log(result);
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  // Sign up with email/password
  singupUser(email: string, password: string) {
    return this.afAuth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        console.log(result);
        /* Call the SendVerificaitonMail() function when new user sign
        up and returns promise */
      }).catch((error) => {
        window.alert(error.message)
      })
  }

  /*  singupUser(email: string, password: string) {
      firebase.auth().createUserWithEmailAndPassword(email, password).catch(
        (error: any) => console.log(error)
      )
    }

    singinUser(email: string, password: string) {
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then(
          (response: any) => console.log(response)
        )
        .catch(
          (error: any) => console.log(error)
        );
    }*/

}
